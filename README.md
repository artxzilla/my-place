## [Android App] Favorite Place

- show nearby place list
- edit location to get nearby place list
- favorite / unfavorite place
- alert when favorite place is nearby

### Kotlin & MVVM & Databinding

with
- android studio 3.0 beta 2
- gradle:3.0.0-beta3
- retrofit2
- glide:4.0.0
- play-services-maps:11.2.0
- play-services-location:11.2.0
- realm-gradle-plugin:3.5.0
- stetho:1.5.0

### License

Panupong Sathapornnuwong [panupong.satha@gmail.com], GitLab, Aug 2017