package com.arzspace.myplace.utils

import android.app.ProgressDialog
import android.content.Context
import android.os.Build
import android.support.v7.app.AlertDialog

/**
 * Created by PANUPONG on 8/23/2017.
 */
class Utils {
    companion object {
        fun getWaitingDialog(context: Context): ProgressDialog {
            return ProgressDialog.show(context, "", "Loading...")
        }

        fun messageDialogUtil(context: Context, title: String, msg: String) {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(msg)
            builder.setPositiveButton("ตกลง") { dialog, which -> dialog.dismiss() }
            builder.create()
            builder.show()
        }

        fun isEmulator(): Boolean {
            return Build.FINGERPRINT.startsWith("generic")
                    || Build.FINGERPRINT.startsWith("unknown")
                    || Build.MODEL.contains("google_sdk")
                    || Build.MODEL.contains("Emulator")
                    || Build.MODEL.contains("Android SDK built for x86")
                    || Build.MANUFACTURER.contains("Genymotion")
                    || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                    || "google_sdk".equals(Build.PRODUCT)
        }
    }
}