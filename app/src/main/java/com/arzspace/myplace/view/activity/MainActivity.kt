package com.arzspace.myplace.view.activity

import android.Manifest
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.arzspace.myplace.R
import com.arzspace.myplace.databinding.ActivityMainBinding
import com.arzspace.myplace.manager.service.FusedLocationService
import com.arzspace.myplace.utils.Utils
import com.arzspace.myplace.view.adapter.ViewPagerAdapter
import com.arzspace.myplace.view.fragment.FavoriteFragment
import com.arzspace.myplace.view.fragment.NearbyFragment


class MainActivity : AppCompatActivity() {
    private val TAG: String = "MainActivity"

    internal lateinit var mServiceIntent: Intent
    private var mFusedLocationService: FusedLocationService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        requirePermission()

        startFusedLocationService()

        setupViewPager(binding.viewpager)
        binding.tabs.setupWithViewPager(binding.viewpager)
    }

    override fun onDestroy() {
        stopService(mServiceIntent)
        super.onDestroy()
    }

    private fun requirePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_NETWORK_STATE)) {

            } else {
                ActivityCompat.requestPermissions(this, Array(1) { Manifest.permission.ACCESS_NETWORK_STATE }, 0x0001)
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.INTERNET)) {

            } else {
                ActivityCompat.requestPermissions(this, Array(1) { Manifest.permission.INTERNET }, 0x0002)
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

            } else {
                ActivityCompat.requestPermissions(this, Array(1) { Manifest.permission.ACCESS_COARSE_LOCATION }, 0x0003)
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {
                ActivityCompat.requestPermissions(this, Array(1) { Manifest.permission.ACCESS_FINE_LOCATION }, 0x0004)
            }
        }
    }

    private fun startFusedLocationService() {
        mFusedLocationService = FusedLocationService(applicationContext)
        mServiceIntent = Intent(applicationContext, FusedLocationService::class.java)
        if (!fusedLocationServiceIsRunning(FusedLocationService::class.java)) {
            startService(mServiceIntent)
        }
    }

    private fun fusedLocationServiceIsRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFrag(NearbyFragment(), getString(R.string.title_nearby))
        adapter.addFrag(FavoriteFragment(), getString(R.string.title_favorite))
        viewPager.adapter = adapter

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                if (position == 0) {
                    NearbyFragment.setUpAdapter()
                } else if (position == 1) {
                    FavoriteFragment.setUpAdapter()
                }
                viewPager.adapter.notifyDataSetChanged()
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }
}
