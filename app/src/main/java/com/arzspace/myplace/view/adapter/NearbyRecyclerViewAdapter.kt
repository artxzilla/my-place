package com.arzspace.myplace.view.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arzspace.myplace.R
import com.arzspace.myplace.databinding.PlaceItemBinding
import com.arzspace.myplace.model.api.GooglePlace
import com.arzspace.myplace.viewmodel.NearbyItemViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


class NearbyRecyclerViewAdapter(private val mContext: Context, private val mPlaces: List<GooglePlace>) : RecyclerView.Adapter<NearbyRecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.place_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding!!.place = mPlaces[position]
        val viewModel = NearbyItemViewModel(mContext, mPlaces[position])

        // map thumbnail
        Glide.with(mContext)
                .load(viewModel.staticMap())
                .apply(RequestOptions.centerCropTransform())
                .into(holder.binding!!.imgMap)

        // bind onclick
        holder.binding!!.imgFavorite.setOnClickListener {
            viewModel.onClickFavoritePlace()
            notifyItemChanged(position)
        }
    }

    override fun getItemCount(): Int {
        return mPlaces.size
    }

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        var binding: PlaceItemBinding? = null

        init {
            binding = DataBindingUtil.bind(mView)
        }
    }

}
