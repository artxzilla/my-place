package com.arzspace.myplace.view.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arzspace.myplace.R
import com.arzspace.myplace.databinding.FragmentFavoriteListBinding
import com.arzspace.myplace.databinding.FragmentNearbyListBinding
import com.arzspace.myplace.model.api.GooglePlace
import com.arzspace.myplace.viewmodel.FavoriteFragmentViewModel


class FavoriteFragment : Fragment() {
    private val TAG: String? = "FavoriteFragment"

    private var binding: FragmentFavoriteListBinding? = null
    private var viewModel: FavoriteFragmentViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorite_list, container, false)
        viewModel = FavoriteFragmentViewModel(context, binding!!.list)

        companionViewModel = viewModel

        viewModel!!.setUpAdapter()

        return binding!!.root
    }

    companion object {
        private var companionViewModel: FavoriteFragmentViewModel? = null

        fun setUpAdapter() {
            companionViewModel!!.setUpAdapter()
        }
    }

}
