package com.arzspace.myplace.view.fragment

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arzspace.myplace.R
import com.arzspace.myplace.databinding.FragmentNearbyListBinding
import com.arzspace.myplace.model.api.GooglePlace
import com.arzspace.myplace.utils.Utils
import com.arzspace.myplace.view.activity.MapActivity
import com.arzspace.myplace.viewmodel.NearbyFragmentViewModel


class NearbyFragment : Fragment() {
    private val TAG: String? = "NearbyFragment"
    private val GOOGLE_PLACE_FLAG: String = "GOOGLE_PLACE"
    private val EDIT_LOCATION_CODE: Int = 0x001

    private var binding: FragmentNearbyListBinding? = null
    private var viewModel: NearbyFragmentViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_nearby_list, container, false)
        viewModel = NearbyFragmentViewModel(context, binding!!.list, GooglePlace())

        companionViewModel = viewModel
        companionLastedPlace = null

        viewModel!!.setUpAdapter(null)

        // bind event change location
        binding!!.floatingChangeLocation.setOnClickListener({
            // is running on emulator
            if (Utils.isEmulator() &&
                    (viewModel!!.getLastedLocation() == null || viewModel!!.getLastedLocation().geometry?.location?.lat == null || viewModel!!.getLastedLocation().geometry?.location?.lng == null)) {
                Log.w(TAG, "Current Location is not working on Emulator")
                val snackBar = Snackbar.make(binding!!.list, "Current Location is not working on Emulator", Snackbar.LENGTH_INDEFINITE)
                snackBar.setAction("Close", { snackBar.dismiss() })
                snackBar.show()
            } else {
                val intent = Intent(context, MapActivity::class.java)
                intent.putExtra(GOOGLE_PLACE_FLAG, viewModel!!.getLastedLocation())
                startActivityForResult(intent, EDIT_LOCATION_CODE)
            }
        })

        return binding!!.root
    }

    companion object {
        private var companionViewModel: NearbyFragmentViewModel? = null
        private var companionLastedPlace: GooglePlace? = null

        fun setUpAdapter() {
            companionViewModel!!.setUpAdapter(companionLastedPlace)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            EDIT_LOCATION_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    // setUpAdapter with latLng
                    companionLastedPlace = data!!.getParcelableExtra(GOOGLE_PLACE_FLAG)
                    viewModel!!.setUpAdapter(data!!.getParcelableExtra(GOOGLE_PLACE_FLAG))
                }
            }
        }
    }
}
