package com.arzspace.myplace.view.activity

import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.arzspace.myplace.BR
import com.arzspace.myplace.R
import com.arzspace.myplace.databinding.ActivityMapBinding
import com.arzspace.myplace.model.api.GooglePlace
import com.arzspace.myplace.utils.Utils
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.io.IOException

class MapActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMapClickListener, View.OnClickListener {

    private val TAG: String = "MapActivity"
    private val GOOGLE_PLACE_FLAG: String = "GOOGLE_PLACE"

    private var binding: ActivityMapBinding? = null
    private var mapFragment: SupportMapFragment? = null
    private var googleMap: GoogleMap? = null
    private var place: GooglePlace? = null
    private var current: LatLng? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_map)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        place = intent.getParcelableExtra(GOOGLE_PLACE_FLAG)
        current = LatLng(place!!.geometry!!.location!!.lat!!, place!!.geometry!!.location!!.lng!!)
        binding!!.setVariable(BR.place, place)
        binding!!.overlayMapaddress.setOnClickListener(this)
        binding!!.executePendingBindings()

        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment!!.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
        this.googleMap!!.setOnMapClickListener(this)
        selectLocation(current)
    }

    override fun onMapClick(latLng: LatLng?) {
        selectLocation(latLng)
    }

    private fun selectLocation(latLng: LatLng?) {
        googleMap!!.clear()
        googleMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        googleMap!!.animateCamera(CameraUpdateFactory.zoomTo(17.0f))
        googleMap!!.addMarker(MarkerOptions().position(latLng!!).title("Select location"))

        try {
            place!!.geometry!!.location!!.lat = latLng.latitude
            place!!.geometry!!.location!!.lng = latLng.longitude

            if (Utils.isEmulator()) {
                Log.w(TAG, "Address is not working on Emulator")
                val snackBar = Snackbar.make(binding!!.overlayMapaddress, "Address is not working on Emulator", Snackbar.LENGTH_INDEFINITE)
                snackBar.setAction("Close", { snackBar.dismiss() })
                snackBar.show()
            } else {
                place!!.address = getAddressByLatLng(latLng.latitude, latLng.longitude)
            }

            binding!!.setVariable(BR.place, place)
            binding!!.executePendingBindings()
        } catch (e: IOException) {
            e.stackTrace
            Log.e(TAG, "IOException : geocoder.getFromLocation", e)
        }
    }

    @Throws(IOException::class)
    private fun getAddressByLatLng(lat: Double, lng: Double): String? {
        val geocoder = Geocoder(this)
        val latitude: Double = lat
        val longitude: Double = lng
        val addresses: List<Address> = geocoder.getFromLocation(latitude, longitude, 1)
        return if (addresses.isNotEmpty()) {
            addresses[0].getAddressLine(0)
        } else {
            "address not found"
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED, Intent())
                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.overlay_mapaddress -> {
                val intent = Intent()
                intent.putExtra(GOOGLE_PLACE_FLAG, place)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
    }
}
