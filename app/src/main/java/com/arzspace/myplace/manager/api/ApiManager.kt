package com.arzspace.myplace.manager.api

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiManager private constructor() {
    private val host = "https://maps.googleapis.com/maps/api/"

    init {
        val gson = GsonBuilder()
                .create()

        val client = OkHttpClient.Builder()
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(host)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
        service = retrofit.create<ApiGooglePlace>(ApiGooglePlace::class.java!!)
    }

    companion object {
        internal var instance: ApiManager = ApiManager()
        lateinit var service: ApiGooglePlace
    }

    fun getService(): ApiGooglePlace {
        return service
    }
}
