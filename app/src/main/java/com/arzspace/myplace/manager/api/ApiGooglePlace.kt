package com.arzspace.myplace.manager.api

import com.arzspace.myplace.model.api.GooglePlace
import com.arzspace.myplace.manager.api.response.ApiResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ApiGooglePlace {
    @GET("place/nearbysearch/json")
    fun getNearbyPlacesService(@QueryMap request: Map<String, String>): Call<ApiResponse<GooglePlace>>
}
