package com.arzspace.myplace.manager.api

import com.arzspace.myplace.manager.api.response.ApiResponse

/**
 * Created by PANUPONG on 8/23/2017.
 */
interface OnServiceListener {
    fun <T> onSuccess(response: ApiResponse<T>)
}