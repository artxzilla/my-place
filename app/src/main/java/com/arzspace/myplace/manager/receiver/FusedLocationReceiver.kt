package com.arzspace.myplace.manager.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.arzspace.myplace.manager.service.FusedLocationService

/**
 * Created by PANUPONG on 8/27/2017.
 */
class FusedLocationReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        context.startService(Intent(context, FusedLocationService::class.java))
    }
}