package com.arzspace.myplace.manager.api

import android.content.Context
import android.util.Log
import com.arzspace.myplace.manager.api.response.ApiResponse
import com.arzspace.myplace.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object ServiceManager {
    private val TAG = "ServiceManager"

    fun <T> service(context: Context, call: Call<ApiResponse<T>>, onServiceListener: OnServiceListener) {
        val waitingDialog = Utils.getWaitingDialog(context)
        waitingDialog.show()
        Log.i("RETROFIT SERVICE", "START")
        call.enqueue(object : Callback<ApiResponse<T>> {
            override fun onResponse(call: Call<ApiResponse<T>>, response: Response<ApiResponse<T>>) {
                if (response.code() == 200) {
                    Log.i(TAG, "SUCCESS")
                    waitingDialog.dismiss()
                    onServiceListener.onSuccess(response.body()!!)
                } else if (response.code() == 204) {
                    Log.i(TAG, "EMPTY BODY")
                    waitingDialog.dismiss()
                    //TODO handle 204
                    Utils.messageDialogUtil(context, "null", "null")
                } else if (response.code() >= 400) {
                    Log.e(TAG, "SERVER ERROR")
                    waitingDialog.dismiss()
                    //TODO handle error
                }
            }

            override fun onFailure(call: Call<ApiResponse<T>>, t: Throwable) {
                t.printStackTrace()
                Log.e(TAG, "onFailure", t)
                waitingDialog.dismiss()
                Utils.messageDialogUtil(context, t.message!!, t.localizedMessage)
            }
        })
    }
}