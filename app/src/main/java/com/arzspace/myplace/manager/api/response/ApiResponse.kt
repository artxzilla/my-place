package com.arzspace.myplace.manager.api.response

/**
 * Created by PANUPONG on 8/23/2017.
 */
class ApiResponse<T> {
    var results: List<T>? = null
    var status: String? = null
}