package com.arzspace.myplace.manager.service

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.IBinder
import android.util.Log
import com.arzspace.myplace.manager.realm.RealmDao
import com.arzspace.myplace.model.realm.FavoritePlace
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import java.util.*


/**
 * Created by PANUPONG on 8/27/2017.
 */
class FusedLocationService : Service {

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var lastLocation: Location? = null

    constructor(applicationContext: Context) : super()

    constructor()

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        startTimer()
        return Service.START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        val broadcastIntent = Intent("com.arzspace.myplace.manager.receiver.FusedLocationReceiver")
        sendBroadcast(broadcastIntent)
        stoptimertask()
    }

    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    fun startTimer() {
        //set a new Timer
        timer = Timer()

        //initialize the TimerTask's job
        initializeTimerTask()

        //schedule the timer, to wake up every 1 second
        timer!!.schedule(timerTask, 1000, 1000)
    }

    fun initializeTimerTask() {
        timerTask = object : TimerTask() {
            @SuppressLint("MissingPermission")
            override fun run() {
                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(applicationContext)
                mFusedLocationClient!!.lastLocation.addOnSuccessListener({
                    location: Location? ->
                    if (location != null && location != lastLocation) {
                        lastLocation = location
                        for (place in RealmDao.favoritePlaceList) {
                            val loc1 = Location("")
                            loc1.latitude = location.latitude
                            loc1.longitude = location.longitude

                            val loc2 = Location("")
                            loc2.latitude = place.lat!!
                            loc2.longitude = place.lng!!

                            if (loc1.distanceTo(loc2) <= 100) {
                                alertNotification(place)
                            }
                        }
                    }
                })
            }
        }
    }

    fun stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun alertNotification(place: FavoritePlace) {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val notificationIntent = Intent("android.media.action.DISPLAY_NOTIFICATION")
        notificationIntent.addCategory("android.intent.category.DEFAULT")
        notificationIntent.putExtra("notificationPlaceId", place.place_id)
        notificationIntent.putExtra("notificationPlaceName", place.place_name)
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)

        val broadcast = PendingIntent.getBroadcast(this, 0x0001, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val cal = Calendar.getInstance()
        cal.add(Calendar.SECOND, 0)
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.timeInMillis, broadcast)
    }
}