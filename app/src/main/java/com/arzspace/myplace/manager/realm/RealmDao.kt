package com.arzspace.myplace.manager.realm

import com.arzspace.myplace.model.api.GooglePlace
import com.arzspace.myplace.model.realm.FavoritePlace
import io.realm.Realm
import io.realm.RealmResults


/**
 * Created by PANUPONG on 8/27/2017.
 */
class RealmDao {
    companion object {
        private val FIELD_NAME_PLACE_ID: String = "place_id"

        fun addOrUpdateFavoritePlaceList(places: List<FavoritePlace>) {
            val realm = Realm.getDefaultInstance()
            realm.executeTransaction({ realm1 -> realm1.copyToRealmOrUpdate(places) })
            realm.close()
        }

        fun deleteFavoritePlace(place: GooglePlace) {
            val realm = Realm.getDefaultInstance()
            realm.executeTransaction({
                realm ->
                val places: RealmResults<FavoritePlace> = realm.where(FavoritePlace::class.java).equalTo(FIELD_NAME_PLACE_ID, place.place_id).findAll()
                places.deleteAllFromRealm()
                realm.close()
            })
        }

        val favoritePlaceList: List<FavoritePlace>
            get() {
                val realm = Realm.getDefaultInstance()
                val favoritePlaceList = realm.copyFromRealm(realm.where(FavoritePlace::class.java).findAll())
                realm.close()
                return favoritePlaceList
            }

        fun isFavoritePlace(placeId: String): Boolean {
            val realm = Realm.getDefaultInstance()
            val place = realm.where(FavoritePlace::class.java).equalTo(FIELD_NAME_PLACE_ID, placeId).findFirst()
            return place != null
        }

        fun clearDatabase() {
            val realm = Realm.getDefaultInstance()
            realm.executeTransaction({ realm1 -> realm1.delete(FavoritePlace::class.java) })
            realm.close()
        }
    }
}