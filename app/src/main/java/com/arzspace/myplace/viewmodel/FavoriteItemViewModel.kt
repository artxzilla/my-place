package com.arzspace.myplace.viewmodel

import android.content.Context
import com.arzspace.myplace.manager.realm.RealmDao
import com.arzspace.myplace.model.api.GooglePlace

/**
 * Created by PANUPONG on 8/27/2017.
 */
class FavoriteItemViewModel(private val mContext: Context, private val mPlace: GooglePlace, private val mPlaces: MutableList<GooglePlace>) {
    fun staticMap(): String {
        return "http://maps.googleapis.com/maps/api/staticmap?" +
                "zoom=17&" +
                "size=500x500&" +
                "maptype=normal&" +
                "markers=icon:${mPlace.icon}|${mPlace.geometry!!.location!!.lat},${mPlace.geometry!!.location!!.lng}"
    }

    fun onClickFavoritePlace() {
        deleteFavaritePlace()
        mPlaces.remove(mPlace)
    }

    fun deleteFavaritePlace() {
        RealmDao.deleteFavoritePlace(mPlace)
    }
}