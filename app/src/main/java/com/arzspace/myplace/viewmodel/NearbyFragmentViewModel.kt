package com.arzspace.myplace.viewmodel

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.arzspace.myplace.R
import com.arzspace.myplace.manager.api.ApiManager
import com.arzspace.myplace.manager.realm.RealmDao
import com.arzspace.myplace.manager.api.response.ApiResponse
import com.arzspace.myplace.manager.api.OnServiceListener
import com.arzspace.myplace.manager.api.ServiceManager
import com.arzspace.myplace.model.api.GooglePlace
import com.arzspace.myplace.view.adapter.NearbyRecyclerViewAdapter
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import java.util.*

/**
 * Created by PANUPONG on 8/27/2017.
 */
class NearbyFragmentViewModel(private val mContext: Context, private val mRecyclerView: RecyclerView, private var mPlace: GooglePlace) : OnServiceListener {
    private val TAG: String? = "NearbyFragmentViewModel"

    private var mFusedLocationClient: FusedLocationProviderClient? = null

    fun setUpAdapter(place: GooglePlace?) {
        if (mRecyclerView is RecyclerView) {
            if (place == null) {
                getNearbyPlacesByCurrentLocation()
            } else {
                getNearbyPlaces(place.geometry?.location?.lat, place.geometry?.location?.lng)

                // set lasted location
                mPlace = place
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun getNearbyPlacesByCurrentLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext)
        mFusedLocationClient!!.lastLocation.addOnSuccessListener({
            location: Location? ->
            if (location != null) {
                getNearbyPlaces(location.latitude, location.longitude)

                // set lasted location
                mPlace = GooglePlace()
                mPlace.geometry!!.location!!.lat = location.latitude
                mPlace.geometry!!.location!!.lng = location.longitude
            }
        })
    }

    private fun getNearbyPlaces(lat: Double?, long: Double?) {
        if (lat != null || long != null) {
            // call google places api
            val request = HashMap<String, String>()
            request.put("location", lat.toString() + "," + long.toString())
            request.put("radius", mContext.resources.getString(R.string.nearby_distance))
            request.put("key", mContext.resources.getString(R.string.google_maps_key))

            val result = ApiManager.instance.getService().getNearbyPlacesService(request)
            ServiceManager.service(mContext, result, this)
        } else {
            Log.w(TAG, "getNearbyPlaces : missing lat or lng")
            setUpPlacesAdapter(emptyList())
        }
    }

    override fun <T> onSuccess(response: ApiResponse<T>) {
        val googlePlaces = response.results as List<GooglePlace>
        if (googlePlaces != null) {
            // todo filter list not political
            setUpPlacesAdapter(googlePlaces)
        }
    }

    private fun setUpPlacesAdapter(places: List<GooglePlace>) {
        for (place in places) {
            place.isFavorite = isFavoritePlace(place.place_id!!)
        }
        mRecyclerView.layoutManager = LinearLayoutManager(mContext)
        mRecyclerView.adapter = NearbyRecyclerViewAdapter(mContext, places)
    }

    private fun isFavoritePlace(placeId: String) : Boolean {
        return RealmDao.isFavoritePlace(placeId)
    }

    fun getLastedLocation(): GooglePlace {
        return mPlace
    }
}