package com.arzspace.myplace.viewmodel

import android.content.Context
import com.arzspace.myplace.manager.realm.RealmDao
import com.arzspace.myplace.model.api.GooglePlace
import com.arzspace.myplace.model.realm.FavoritePlace

/**
 * Created by PANUPONG on 8/27/2017.
 */
class NearbyItemViewModel(private val mContext: Context, private val mPlace: GooglePlace) {
    fun staticMap(): String {
        return "http://maps.googleapis.com/maps/api/staticmap?" +
                "zoom=17&" +
                "size=500x500&" +
                "maptype=normal&" +
                "markers=icon:${mPlace.icon}|${mPlace.geometry!!.location!!.lat},${mPlace.geometry!!.location!!.lng}"
    }

    fun onClickFavoritePlace() {
        if (mPlace.isFavorite) {
            // delete
            deleteFavaritePlace()
        } else {
            // add
            addFavoritePlace()
        }
        mPlace.isFavorite = !mPlace.isFavorite
    }

    fun addFavoritePlace() {
        var places: List<FavoritePlace> = emptyList()
        val place = FavoritePlace()
        place.id = FavoritePlace.cachedNextId!!
        place.place_id = mPlace.place_id!!
        place.place_name = mPlace.name
        place.vicinity = mPlace.vicinity
        place.lat = mPlace.geometry?.location?.lat
        place.lng = mPlace.geometry?.location?.lng
        places += place
        RealmDao.addOrUpdateFavoritePlaceList(places)
    }

    fun deleteFavaritePlace() {
        RealmDao.deleteFavoritePlace(mPlace)
    }
}