package com.arzspace.myplace.viewmodel

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.arzspace.myplace.manager.realm.RealmDao
import com.arzspace.myplace.model.api.GooglePlace
import com.arzspace.myplace.view.adapter.FavoriteRecyclerViewAdapter

/**
 * Created by PANUPONG on 8/27/2017.
 */
class FavoriteFragmentViewModel(private val mContext: Context, private val mRecyclerView: RecyclerView) {
    private val TAG: String? = "FavoriteFragmentViewModel"

    fun setUpAdapter() {
        if (mRecyclerView is RecyclerView) {
            // get favoritePlace
            val favoritePlaces = RealmDao.favoritePlaceList
            var places = emptyList<GooglePlace>()
            for (favoritePlace in favoritePlaces) {
                val place: GooglePlace = GooglePlace()
                place.place_id = favoritePlace.place_id
                place.name = favoritePlace.place_name
                place.vicinity = favoritePlace.vicinity
                place.geometry!!.location!!.lat = favoritePlace.lat
                place.geometry!!.location!!.lng = favoritePlace.lng
                place.isFavorite = true

                places += place
            }

            mRecyclerView.layoutManager = LinearLayoutManager(mContext)
            mRecyclerView.adapter = FavoriteRecyclerViewAdapter(mContext, places)
        }
    }
}