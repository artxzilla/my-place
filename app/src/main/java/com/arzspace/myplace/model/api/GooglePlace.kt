package com.arzspace.myplace.model.api

import android.os.Parcel
import android.os.Parcelable

data class GooglePlace(
        var geometry: Geometry?,
        var icon: String?,
        var id: String?,
        var name: String?,
        var place_id: String?,
        var reference: String?,
        var vicinity: String?,
        var address: String?,
        var url: String?,
        var isFavorite: Boolean
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readParcelable<Geometry>(Geometry::class.java.classLoader),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            1 == source.readInt()
    )

    constructor() : this(
            Geometry(),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            false
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(geometry, 0)
        writeString(icon)
        writeString(id)
        writeString(name)
        writeString(place_id)
        writeString(reference)
        writeString(vicinity)
        writeString(address)
        writeString(url)
        writeInt((if (isFavorite) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<GooglePlace> = object : Parcelable.Creator<GooglePlace> {
            override fun createFromParcel(source: Parcel): GooglePlace = GooglePlace(source)
            override fun newArray(size: Int): Array<GooglePlace?> = arrayOfNulls(size)
        }
    }

}