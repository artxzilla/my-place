package com.arzspace.myplace.model.realm

import io.realm.Realm
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey

open class FavoritePlace(
        @PrimaryKey
        var id: Long = 0,
        var place_id: String = "",
        var place_name: String? = null,
        var vicinity: String? = null,
        var lat: Double? = null,
        var lng: Double? = null,
        var is_nearby: Boolean = false
) : RealmObject() {
    companion object {
        @Ignore var cachedNextId: Long? = null
            get() {
                val nextId = if (field != null) field?.plus(1)
                else Realm.getDefaultInstance()?.where(FavoritePlace::class.java)?.max("id")?.toLong()?.plus(1) ?: 1
                FavoritePlace.cachedNextId = nextId
                return nextId
            }
    }
}