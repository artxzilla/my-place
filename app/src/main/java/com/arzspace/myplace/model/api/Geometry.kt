package com.arzspace.myplace.model.api

import android.os.Parcel
import android.os.Parcelable

data class Geometry(
        var location: Location? = null
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readParcelable<Location>(Location::class.java.classLoader)
    )

    constructor() : this(
            Location()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(location, 0)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Geometry> = object : Parcelable.Creator<Geometry> {
            override fun createFromParcel(source: Parcel): Geometry = Geometry(source)
            override fun newArray(size: Int): Array<Geometry?> = arrayOfNulls(size)
        }
    }
}